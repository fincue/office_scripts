#!/bin/bash

set -e

#----------------------------------------------------
echo -e "INSTALL PRITUNL CLIENT"
#----------------------------------------------------
sudo tee /etc/apt/sources.list.d/pritunl.list << EOF
deb http://repo.pritunl.com/stable/apt bionic main
EOF

sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com --recv 7568D9BB55FF9E5287D586017AE645C0CF8E292A
sudo apt-get update -y
sudo apt-get install -y pritunl-client-electron


#----------------------------------------------------
echo -e "CONFIGURE FINCUE DNS"
#----------------------------------------------------
sudo tee /etc/systemd/resolved.conf << EOF
[Resolve]
DNS=10.219.0.2
FallbackDNS=1.1.1.1 8.8.8.8
Domains=fin.local fin.prod fin.stage microfin.local
LLMNR=no
MulticastDNS=no
DNSSEC=no
Cache=yes
DNSStubListener=yes
EOF

sudo tee /etc/nsswitch.conf << EOF
passwd:         files systemd
group:          files systemd
shadow:         files
gshadow:        files
hosts:          files mdns4_minimal dns
networks:       files
protocols:      db files
services:       db files
ethers:         db files
rpc:            db files
netgroup:       nis
EOF

sudo systemd restart systemd-resolved
#----------------------------------------------------
echo -e "!!! DONE !!!"
